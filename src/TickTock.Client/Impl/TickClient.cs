﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using Newtonsoft.Json;
using TickTock.Client.Model;

namespace TickTock.Client.Impl
{
  public class TickClient : ITickClient
  {
    private readonly TickSettings _settings;

    public TickClient(TickSettings settings)
    {
      _settings = settings;
    }

    public async Task<IEnumerable<Tick>> Query()
    {
      using (var client = new HttpClient())
      {
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
        var resp = await client.GetStringAsync("/api/ticks");
        return JsonConvert.DeserializeObject<IList<Tick>>(resp);
      } 
    }

    public async Task Tick()
    {

      using (var client = new HttpClient())
      {
        client.DefaultRequestHeaders.Add("Content-Type", "application/json");
        client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

        var tick =
          JsonConvert.SerializeObject(new Tick
          {
            AppVersion = _settings.AppVersion,
            Hostname = Environment.MachineName,
            TickTime = DateTime.UtcNow
          });

        var content = new StringContent(tick);

        await client.PostAsync("/api/ticks", content);
      }
    }
  }
}