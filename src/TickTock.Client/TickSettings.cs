﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TickTock.Client
{
  public class TickSettings
  {
    public string Api { get; set; }

    public string AppVersion { get; set; }
  }
}
