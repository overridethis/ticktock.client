﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TickTock.Client.Model
{
  public class Tick
  {
    public int TickId { get; set; }
    public DateTime TickTime { get; set; }
    public string AppVersion { get; set; }
    public string Hostname { get; set; }
  }
}
