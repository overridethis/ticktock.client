﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TickTock.Client.Model;

namespace TickTock.Client
{
  public interface ITickClient
  {
    Task<IEnumerable<Tick>> Query();

    Task Tick();
  }
}
